<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class SendFormComponent extends \CBitrixComponent
{
    public function sendData($data, $sessid)
    {
        foreach ($data as $key => $value) {
            if ($value == '') {
                $result[] = array($key => $value);
            }
        }
        if (isset($result)) {
            return $result;
        } else {
            SendFormComponent::checkData($data, $sessid);
        }
    }

    public function checkData($data, $sessid)
    {
        if (CModule::IncludeModule('iblock')) {
            $arSort = array('sort' => 'asc');
            $arFilter = array('IBLOCK_ID' => LEAD_IBLOCK_ID, 'PROPERTY_SESSID' => $sessid);
            $arSelect = array("ID", "PROPERTY_SESSID");
            $res = CIBlockElement::GetList($arSort, $arFilter, $arSelect);

            if ($ar_fields = $res->GetNext()) {
                SendFormComponent::updateData($data, $ar_fields['ID']);
            } else {
                SendFormComponent::addData($data, $sessid);
            }
        }
    }

    public function updateData($data, $idElement)
    {
        if (CModule::IncludeModule('iblock')) {
            CIBlockElement::SetPropertyValueCode($idElement, 'NAME', $data['InputName']);
            CIBlockElement::SetPropertyValueCode($idElement, 'LAST_NAME', $data['InputLastName']);
            CIBlockElement::SetPropertyValueCode($idElement, 'PHONE', $data['InputPhone']);
        }
    }

    public function addData($data, $sessid)
    {
        if (CModule::IncludeModule('iblock')) {
            $el = new CIBlockElement;
            $arLoadProductArray = Array(
                "IBLOCK_SECTION_ID" => false,
                "IBLOCK_ID" => LEAD_IBLOCK_ID,
                "PROPERTY_VALUES" => array(
                    "NAME" => $data['InputName'],
                    "LAST_NAME" => $data['InputLastName'],
                    "PHONE" => $data['InputPhone'],
                    "SESSID" => $sessid
                ),
                "NAME" => $data['InputName'] . ' ' . $sessid,
                "ACTIVE" => "Y",
            );
            if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
                //тут должна быть обработка успешной отправки
            } else {
                //тут должна быть обработка ошибки
            }
        }
    }

    public function showDataBySessid($sessid)
    {
        if (CModule::IncludeModule('iblock')) {
            $arSort = array('sort' => 'asc');
            $arFilter = array('IBLOCK_ID' => LEAD_IBLOCK_ID, 'PROPERTY_SESSID' => $sessid);
            $arSelect = array("ID", "PROPERTY_NAME", "PROPERTY_LAST_NAME", "PROPERTY_PHONE");
            $res = CIBlockElement::GetList($arSort, $arFilter, $arSelect);

            if ($ar_fields = $res->GetNext()) {
                $dataArray = array(
                    "NAME" => $ar_fields['PROPERTY_NAME_VALUE'],
                    "LAST_NAME" => $ar_fields['PROPERTY_LAST_NAME_VALUE'],
                    "PHONE" => $ar_fields['PROPERTY_PHONE_VALUE']
                );
                return $dataArray;
            } else {
                $dataArray = '';
                return $dataArray;
            }
        }
    }
}

?>