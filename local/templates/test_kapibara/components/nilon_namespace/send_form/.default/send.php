<? require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php"); ?>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

CBitrixComponent::includeComponentClass("nilon_namespace:send_form");

$result = SendFormComponent::sendData($_POST, bitrix_sessid());

if (isset($result)) {
    echo json_encode(array('ERROR' => 'YES'));
} else {
    echo json_encode(array('ERROR' => 'NO'));
}
?>