$(document).ready(function () {

    $('.send-form').submit(function (event) {
        event.preventDefault();
        var form = $(this);
        var submitBtn = form.find('[type="submit"]');

        var InputName = form.find('input[name="InputName"]');
        var InputLastName = form.find('input[name="InputLastName"]');
        var InputPhone = form.find('input[name="InputPhone"]');

        submitBtn.attr('disabled', true);
        submitBtn.text('...');

        $.ajax({
            type: 'POST',
            url: BX.message('TEMPLATE_PATH') + '/send.php',
            data: form.serializeArray(),
            success: function (data) {
                var mes = JSON.parse(data);
                if (mes.ERROR == 'YES') {
                    console.log('Заполните все поля');
                    //тут должна быть красивая обработка ошибки
                    submitBtn.text('Ошибка!');
                    setTimeout(function () {
                        $('.send-form').fadeOut(200);
                        setTimeout(function () {
                            $('.send-form').fadeIn(200);
                            InputName.val('');
                            InputLastName.val('');
                            InputPhone.val('');
                            submitBtn.attr('disabled', false);
                            submitBtn.text('Отправить');
                        }, 300);
                    }, 3000);
                } else {
                    console.log('Отправлено');
                    //тут должна быть красивая обработка ошибки
                    submitBtn.text('Отправлено!');
                    setTimeout(function () {
                        $('.send-form').fadeOut(200);
                        setTimeout(function () {
                            $('.send-form').fadeIn(200);
                            InputName.val('');
                            InputLastName.val('');
                            InputPhone.val('');
                            submitBtn.attr('disabled', false);
                            submitBtn.text('Отправить');
                        }, 200);
                    }, 2000);
                }
            }
        });
    });

    function downloadCSV(args) {
        var data_table = 'Имя;Фамилия;Телефон;' + '\n';
        data_table += BX.message('EXPORT_DATA_TABLE');

        console.log(data_table);

        var filename, link;
        var csv = data_table;

        if (csv == null) return;

        filename = args.filename || 'export.csv';

        if (!csv.match(/^data:text\/csv/i)) {
            csv = 'data:text/csv;charset=utf-8,\uFEFF' + csv;
        }
        data_table = encodeURI(csv);

        link = document.createElement('a');
        link.setAttribute('href', data_table);
        link.setAttribute('download', filename);
        link.click();
    }

    $(".export-button").click(function () {
        downloadCSV({filename: "export-table.csv"});
    });

});