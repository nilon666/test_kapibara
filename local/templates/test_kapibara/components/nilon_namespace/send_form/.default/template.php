<? require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php"); ?>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
CJSCore::Init(array("ajax"));
?>
<div class="row">
    <div class="col-md-3 col-md-offset-4 col-xs-12">
        <form class="send-form">
            <div class="form-group">
                <label><?= GetMessage("FORM_INPUT_NAME") ?></label>
                <input type="text" class="form-control" name="InputName"
                       placeholder="<?= GetMessage("HOLD_INPUT_NAME") ?>">
            </div>
            <div class="form-group">
                <label><?= GetMessage("FORM_INPUT_LAST_NAME") ?></label>
                <input type="text" class="form-control" name="InputLastName"
                       placeholder="<?= GetMessage("HOLD_INPUT_LAST_NAME") ?>">
            </div>
            <div class="form-group">
                <label><?= GetMessage("FORM_INPUT_PHONE") ?></label>
                <input type="text" class="form-control" name="InputPhone"
                       placeholder="<?= GetMessage("HOLD_INPUT_PHONE") ?>">
            </div>
            <button type="submit" class="btn btn-primary btn-block"><?= GetMessage("FORM_INPUT_BUTTON") ?></button>
        </form>
    </div>
</div>
<br>
<? if (!empty($arResult)) { ?>
    <div class="row">
        <div class="col-md-3 col-md-offset-4 col-xs-12">
            <table class="table table-hover">
                <tbody>
                <tr>
                    <th scope="row"><?= GetMessage("TABLE_INPUT_NAME") ?></th>
                    <td><?= $arResult['NAME'] ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= GetMessage("TABLE_INPUT_LAST_NAME") ?></th>
                    <td><?= $arResult['LAST_NAME'] ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= GetMessage("TABLE_INPUT_PHONE") ?></th>
                    <td><?= $arResult['PHONE'] ?></td>
                </tr>
                </tbody>
            </table>
            <button type="submit" class="export-button btn btn-primary btn-block"><?= GetMessage("TABLE_INPUT_BUTTON") ?></button>
        </div>
    </div>
    <script>BX.message({EXPORT_DATA_TABLE: "<?=$arResult['NAME'].';'.$arResult['LAST_NAME'].';'.$arResult['PHONE'].';'?>"});</script>
<? } ?>
<script>BX.message({TEMPLATE_PATH: '<? echo $this->GetFolder(); ?>'});</script>


