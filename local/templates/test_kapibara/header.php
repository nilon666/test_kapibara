<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Page\Asset;
global $APPLICATION;
?>
<!DOCTYPE html>
<html>
<head>
    <? $APPLICATION->ShowHead(); ?>
    <title><? $APPLICATION->ShowTitle(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?
    Asset::getInstance()->addCss("//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css");
    Asset::getInstance()->addJs("//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js");
    Asset::getInstance()->addJs("//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js");
    Asset::getInstance()->addString("<link rel='shortcut icon' type='image/x-icon' href='/favicon.ico' />");
    ?>
</head>
<body>
<div id="panel"><? $APPLICATION->ShowPanel(); ?></div>

<div class="container">
    <? $APPLICATION->IncludeComponent('nilon_namespace:send_form', array('IBLOCK_ID' => LEAD_IBLOCK_ID)); ?>